add:
	make next
	git commit feed.xml -m "Mise a jour du flux"

next:
	docker run -v $(PWD):/sources -w /sources -ti php:7.3-cli /bin/bash -c "/sources/run"

docker-bash:
	docker run -v $(PWD):/sources -w /sources -ti php:7.3-cli /bin/bash
