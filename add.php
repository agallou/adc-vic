<?php

$numero = $argv[1];

echo "Ajout numéro " . $numero . PHP_EOL;

$url = "http://podcast.captainweb.net/l-apero-du-captain-" . $numero;
$date = new \DateTime();

$file = __DIR__ . DIRECTORY_SEPARATOR . 'feed.xml';

$domDocument = new \DomDocument();
$domDocument->formatOutput = true;
$domDocument->preserveWhiteSpace = false;
$domDocument->load($file);

$xpathRootPubDate = new \DomXpath($domDocument);
$pubDateList = $xpathRootPubDate->query('//channel/pubDate');

if ($pubDateList->length != 1) {
  echo 'Erreur lecture XML (pubDate)' . PHP_EOL.
  exit(1);
}

$xpathRootLastBuildDate = new \DomXpath($domDocument);
$lastBuildDateList = $xpathRootLastBuildDate->query('//channel/lastBuildDate');

if ($lastBuildDateList->length != 1) {
  echo 'Erreur lecture XML (pubDate)' . PHP_EOL.
  exit(1);
}


$rssList = $domDocument->getElementsByTagName('rss');
if ($rssList->length != 1) {
  echo 'Erreur lecture XML' . PHP_EOL.
  exit(1);
}

$itemList = $domDocument->getElementsByTagName('item');
if ($itemList->length <= 0) {
  echo 'Erreur lecture XML' . PHP_EOL.
  exit(1);
}

$pubDate = $pubDateList->item(0);
$pubDate->textContent = $date->format(\DateTime::RFC2822);

$lastBuildDate = $lastBuildDateList->item(0);
$lastBuildDate->textContent = $date->format(\DateTime::RFC2822);

$item = $domDocument->createElement('item');

$title = $domDocument->createElement('title');
$title->textContent = 'ADC ' . $numero;
$item->appendChild($title);

$link = $domDocument->createElement('link');
$item->appendChild($link);

$guid = $domDocument->createElement('guid');
$guid->textContent = $url;
$item->appendChild($guid);

$desc = $domDocument->createElement('description');
$item->appendChild($desc);

$enc = $domDocument->createElement('enclosure');
$enc->setAttribute('url', $url . '.mp3');
$item->appendChild($enc);

$cat = $domDocument->createElement('category');
$cat->textContent = "Podcasts";
$item->appendChild($cat);

$pub = $domDocument->createElement('pubDate');
$pub->textContent = $date->format(\DateTime::RFC2822);
$item->appendChild($pub);

$itemList->item(0)->parentNode->insertBefore($item, $itemList->item(0));
$domDocument->save($file);
echo 'File "' . $file . '" written to disk' . PHP_EOL;

