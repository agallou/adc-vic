<?php

$file = __DIR__ . DIRECTORY_SEPARATOR . 'feed.xml';

$domDocument = new \DomDocument();
$domDocument->load($file);

$itemList = $domDocument->getElementsByTagName('item');
if ($itemList->length <= 0) {
  echo 'Erreur lecture XML' . PHP_EOL.
  exit(1);
}

$firstItem = $itemList->item(0);
$latestName = $firstItem->getElementsByTagName('title')->item(0)->textContent;
$latestId = (int)trim(str_replace('ADC', '', $latestName));
$nextId = $latestId + 1;

echo $nextId . PHP_EOL;

